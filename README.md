sendsms

This is a quick and dirty script to send sms reminders. I'm currently using it
to co-ordinate my roleplaying games, but I'm hoping it might be usful for other
things as well.

It relies on a yaml formatted config file and (see config.yaml.sample), a yaml
file listing the phone numbers to send the notifications to (see
recipients.yaml.sample).

I use a bash script to actually run it (see send_reminders.bash.sample)

You'll need a twilio account so that you can supply an account SID and key.

I use a function at twilio to forward responses to my email, that's not
documented here, but there examples at twilio.
