#!/bin/env python3
# <one line to give the program's name and a brief idea of what it does.>
# sendsms - a program to send reminders via twilio's sms gateway.
# Copyright (C) 2022 Neill Cox (neill.cox@ingenious.com.au)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import os
import sys
import yaml

import click
from twilio.rest import Client


# Create a custom logger
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

c_handler = logging.StreamHandler()
c_format = logging.Formatter("%(name)s - %(levelname)s - %(message)s")
c_handler.setFormatter(c_format)
log.addHandler(c_handler)

account_sid = os.environ["ACCOUNT_SID"]
auth_token = os.environ["AUTH_TOKEN"]
sender = os.environ["SENDER"]
dry_run = os.environ.get("DRY_RUN", "true")
# when = os.environ["WHEN"]
# where = os.environ.get("WHERE", "online")

# dry_run = dry_run.lower() != "false"


def send_message(sender, recipient, body, dry_run=False):

    client = Client(account_sid, auth_token)

    if not dry_run:
        client.messages.create(body=body, from_=sender, to=recipient)

    log.info(f"Sending message to:{recipient}")
    log.info(f"{body}")

    if dry_run:
        log.info("This was a dry run so no message was sent")

@click.option(
    "--message",
    default=(
        "Hi {name}, just a quick reminder that there is a roleplaying session "
        "at 7:30pm online please let me know if you can make it (or not). "
        "Cheers, Neill"
        )
    )
@click.option(
    "--recipients",
    help="File containing a YAML formatted list of recipients"
    )
@click.option("--dry-run", is_flag=True)
@click.command
def sms_reminders(message, recipients, dry_run):
    recipient_list = []
    with open(recipients, "r") as recipients:
        recipient_list = yaml.safe_load(recipients)

    for recipient in recipient_list:
        name = recipient["name"]
        msg = message.format(name=name)
        send_message(sender, recipient["mobile"], msg, dry_run=dry_run)


if __name__ == "__main__":
    sms_reminders()
